import SpotifyWebApi from 'spotify-web-api-node';

const scopes = [
		'user-read-private',
		'user-read-email',
		'playlist-read-collaborative',
		'playlist-read-private',
		'user-library-read',
		'user-top-read',
		'user-read-recently-played',
		'user-read-private',
	],
	host = window.location.origin,
	redirectUri = host + window.location.pathname + 'callback.html',
	clientId = 'ee933b69611d401e989aaf6948277cd3',
	state = 'some-state-of-my-choice';

export interface SpotifyUser {
	displayName: string;
}

export class SpotifyAPI {
	spotifyApi: SpotifyWebApi;

	constructor() {
		this.spotifyApi = new SpotifyWebApi({
			redirectUri: redirectUri,
			clientId: clientId,
		});
	}

	async obtainToken(): Promise<boolean> {
		// Many thanks to JMPerez for spotify dedup (https://github.com/JMPerez/spotify-dedup) which served as a great reference for implementing the implict grant authorization flow
		const promise = new Promise<string>((resolve, reject) => {
			let authWindow: Window | null = null;
			let pollAuthWindowClosed: NodeJS.Timeout | null = null;

			function receiveMessage(event: { origin: string; data: unknown }) {
				if (pollAuthWindowClosed) {
					clearInterval(pollAuthWindowClosed);
				}

				if (event.origin !== host) {
					reject({
						message: `Origin ${event.origin} does not match ${host}`,
					});
					return;
				}

				if (authWindow !== null) {
					if (!authWindow.closed) {
						authWindow.close();
					}
					authWindow = null;
				}

				window.removeEventListener('message', receiveMessage, false);

				// TODO: manage case when the user rejects the oauth
				// or the oauth fails to obtain a token
				resolve(event.data as string);
			}

			window.addEventListener('message', receiveMessage, false);

			const width = 400;
			const height = 600;
			const left = window.screen.width / 2 - width / 2;
			const top = window.screen.height / 2 - height / 2;

			// We gotta replace the response_type to be able to follow the 'implicit grant' auth flow
			const authUrl = this.spotifyApi
				.createAuthorizeURL(scopes, state)
				.replace('response_type=code', 'response_type=token');

			authWindow = window.open(
				authUrl,
				'Spotify',
				`menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=${width}, height=${height}, top=${top}, left=${left}`
			);

			pollAuthWindowClosed = setInterval(() => {
				if (authWindow?.closed ?? false) {
					clearInterval(pollAuthWindowClosed!);
					reject({ message: 'access_denied' });
				}
			}, 1000);
		});

		this.spotifyApi.setAccessToken(await promise);
		return true;
	}

	async isSignedIn(): Promise<boolean> {
		return this.spotifyApi.getAccessToken() !== undefined;
	}
	async getAuthUrl(): Promise<string> {
		throw new Error('Not Implemented');
	}

	async getUser(): Promise<SpotifyUser> {
		const user = await this.spotifyApi.getMe();
		// if (!user) {
		// 	throw { statusCode: 400 };
		// }
		return { displayName: user.body.display_name ?? '' };
	}

	async getMyRecentlyPlayedTracks(): Promise<
		SpotifyApi.UsersRecentlyPlayedTracksResponse
	> {
		return (
			await this.spotifyApi.getMyRecentlyPlayedTracks({
				limit: 50,
			})
		).body;
	}

	async getMyTopArtists(): Promise<SpotifyApi.UsersTopArtistsResponse> {
		return (
			await this.spotifyApi.getMyTopArtists({
				limit: 50,
			})
		).body;
	}

	async getPlaylistTracks(
		playlist_id: string
	): Promise<SpotifyApi.PlaylistTrackResponse> {
		return (await this.spotifyApi.getPlaylistTracks(playlist_id)).body;
	}

	async getTrackAudioFeatures(
		track_id: string
	): Promise<SpotifyApi.AudioFeaturesResponse> {
		return (await this.spotifyApi.getAudioFeaturesForTrack(track_id)).body;
	}

	async getArtist(artist_id: string): Promise<SpotifyApi.ArtistObjectFull> {
		return (await this.spotifyApi.getArtist(artist_id)).body;
	}

	async getUserPlaylists(): Promise<SpotifyApi.ListOfUsersPlaylistsResponse> {
		var finalResult;
		var first = (await this.spotifyApi.getUserPlaylists({ limit: 50 }))
			.body;

		if (first.next) {
			var second = (
				await this.spotifyApi.getUserPlaylists({
					offset: first.limit,
					limit: 50,
				})
			).body;

			finalResult = {
				href: first.href,
				items: first.items.concat(second.items),
				limit: first.limit + second.limit,
				next: second.next,
				offset: 0,
				total: first.total + second.total,
			} as SpotifyApi.ListOfUsersPlaylistsResponse;
		} else {
			finalResult = first;
		}
		return finalResult;
	}

	// async logOut(): Promise<void> {
	// 	// return this.makeRequest(nameof('logOut')).then((res) => {});
	// }

	parseReleaseDate(
		release_date: string,
		release_data_precision: string
	): Date {
		var date_parts = release_date.split('-');
		var parsed_parts = date_parts.map((part) => parseInt(part));
		if (release_data_precision === 'day') {
			return new Date(parsed_parts[0], parsed_parts[1], parsed_parts[2]);
		} else if (release_data_precision === 'month') {
			return new Date(parsed_parts[0], parsed_parts[1]);
		} else if (release_data_precision === 'year') {
			return new Date(parsed_parts[0], 1);
		} else {
			throw new Error('Unhandled Item type');
		}
	}
}

export const spotify = new SpotifyAPI();
