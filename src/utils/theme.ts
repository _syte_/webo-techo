import { createMuiTheme } from '@material-ui/core/styles';
import { green, orange } from '@material-ui/core/colors';

export const mainTheme = createMuiTheme({
	palette: {
		type: 'dark',
		primary: green,
		secondary: orange,
	},
});

export const altTheme = createMuiTheme({
	palette: {
		type: 'light',
		primary: green,
		secondary: orange,
	},
});
