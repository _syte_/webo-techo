import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit';
import { AppDispatch } from './store';
import { RootState } from './rootReducer';
import { spotify, SpotifyUser } from '../utils/SpotifyAPI';
import { useSelector, useDispatch } from 'react-redux';
import { useEffect } from 'react';

export interface UserWrapper {
	user?: SpotifyUser;
	errorMessage?: string;
}

const initialState: UserWrapper = {};

export const getUser = createAsyncThunk<
	// Return type of the payload creator
	SpotifyUser | undefined,
	// First argument to the payload creator
	void,
	{
		dispatch: AppDispatch;
		state: RootState;
		extra: {
			jwt: string;
		};
	}
>('users/get', async (_, thunk) => {
	// thunk.dispatch;
	const currentUser = thunk.getState().user;
	if (currentUser.user) {
		return currentUser.user;
	} else {
	}

	const signedIn = await spotify.isSignedIn();
	if (signedIn) {
		return await spotify.getUser();
	} else {
		return undefined;
	}
});

const userSlice = createSlice({
	name: 'user',
	initialState,
	reducers: {
		updateUser(state, action: PayloadAction<SpotifyUser>) {
			state.user = action.payload;
		},
		// passwordSet(state, action: PayloadAction) {
		// 	if (state.user) {
		// 		state.user.hasPassword = true;
		// 	}
		// },
	},
	extraReducers: (builder) => {
		builder.addCase(getUser.fulfilled, (state, { payload }) => {
			state.user = payload;
		});
		builder.addCase(getUser.rejected, (state, action) => {
			// if (action.payload) {
			// 	// Since we passed in `MyKnownError` to `rejectType` in `updateUser`, the type information will be available here.
			// 	state.error = action.payload.errorMessage;
			// } else {
			// 	state.error = action.error;
			// }
		});
	},
});

export function useUser(): SpotifyUser | undefined {
	const dispatch = useDispatch();
	const user = useSelector((state: RootState) => state.user.user);
	useEffect(() => {
		dispatch(getUser());
	}, [dispatch]);

	return user;
}

export const { updateUser } = userSlice.actions;

export default userSlice.reducer;
