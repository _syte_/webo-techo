import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export enum AppTheme {
	Light,
	Dark,
}

const initialState: AppTheme = AppTheme.Light as AppTheme;

const themeSlice = createSlice({
	name: 'theme',
	initialState,
	reducers: {
		changeTheme(state, action: PayloadAction<AppTheme>) {
			return action.payload;
		},
		toggleTheme(state, action: PayloadAction<void>) {
			return state === AppTheme.Light ? AppTheme.Dark : AppTheme.Light;
		},
	},
});

export const { changeTheme, toggleTheme } = themeSlice.actions;

export default themeSlice.reducer;
