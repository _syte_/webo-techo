import * as React from 'react';

import {
	Typography,
	makeStyles,
	createStyles,
	Theme,
	Card,
	CardContent,
	CardMedia,
	Tooltip,
} from '@material-ui/core';
import { spotify } from '../utils/SpotifyAPI';
import { useState, useEffect } from 'react';

export type PlaylistItem =
	| SpotifyApi.TrackObjectFull
	| SpotifyApi.AlbumObjectSimplified
	| SpotifyApi.ArtistObjectSimplified;

export function isArtist(
	toBeDetermined: PlaylistItem
): toBeDetermined is SpotifyApi.ArtistObjectSimplified {
	return (
		(toBeDetermined as SpotifyApi.ArtistObjectSimplified).type === 'artist'
	);
}

export function isAlbum(
	toBeDetermined: PlaylistItem
): toBeDetermined is SpotifyApi.AlbumObjectSimplified {
	return (
		(toBeDetermined as SpotifyApi.AlbumObjectSimplified).type === 'album'
	);
}

const getItemMedia = async (item: PlaylistItem): Promise<string> => {
	if (!item) {
		return '';
	}
	if (isTrack(item)) {
		return item.album.images[0].url;
	} else if (isAlbum(item)) {
		return item.images[0].url;
	} else if (isArtist(item)) {
		return (await spotify.getArtist(item.id)).images[0].url;
	} else {
		throw new Error('Unhandled Item type');
	}
};

const getItemTitle = (item: PlaylistItem) => {
	if (!item) {
		return;
	}
	if (isTrack(item)) {
		return item.name;
	} else if (isAlbum(item)) {
		return item.name;
	} else if (isArtist(item)) {
		return item.name;
	} else {
		throw new Error('Unhandled Item type');
	}
};

const getItemTitleLink = (item: PlaylistItem) => {
	if (!item) {
		return;
	}
	return item.external_urls.spotify;
};

const getItemMainSubtitle = (item: PlaylistItem) => {
	if (!item) {
		return;
	}
	if (isTrack(item)) {
		return item.album.name;
	} else if (isAlbum(item)) {
		return item.artists[0].name;
	} else if (isArtist(item)) {
		return '';
	} else {
		throw new Error('Unhandled Item type');
	}
};

const getItemSecondarySubtitle = (item: PlaylistItem) => {
	if (!item) {
		return;
	}
	if (isTrack(item)) {
		return item.artists[0].name;
	} else if (isAlbum(item)) {
		return '';
	} else if (isArtist(item)) {
		return '';
	} else {
		throw new Error('Unhandled Item type');
	}
};

const getItemMainSubtitleLink = (item: PlaylistItem) => {
	if (!item) {
		return;
	}
	if (isTrack(item)) {
		return item.album.external_urls.spotify;
	} else if (isAlbum(item)) {
		return item.artists[0].external_urls.spotify;
	} else if (isArtist(item)) {
		return '';
	} else {
		throw new Error('Unhandled Item type');
	}
};

const getItemSecondarySubtitleLink = (item: PlaylistItem) => {
	if (!item) {
		return;
	}
	if (isTrack(item)) {
		return item.artists[0].external_urls.spotify;
	} else if (isAlbum(item)) {
		return '';
	} else if (isArtist(item)) {
		return '';
	} else {
		throw new Error('Unhandled Item type');
	}
};

const hasRelease = (item: PlaylistItem) => isAlbum(item) || isTrack(item);

const getItemRelease = (item: PlaylistItem) => {
	if (!item) {
		return;
	}
	if (isTrack(item)) {
		return spotify.parseReleaseDate(
			item.album.release_date,
			item.album.release_date_precision
		);
	} else if (isAlbum(item)) {
		return spotify.parseReleaseDate(
			item.release_date,
			item.release_date_precision
		);
	} else {
		throw new Error('Unhandled Item type');
	}
};

export function isTrack(
	toBeDetermined: PlaylistItem
): toBeDetermined is SpotifyApi.TrackObjectFull {
	return (toBeDetermined as SpotifyApi.TrackObjectFull).type === 'track';
}

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		media: {
			maxHeight: 200,
			width: 'auto',
			// paddingTop: '56.25%', // 16:9
		},
		content: {
			display: 'flex',
			flexDirection: 'column',
			alignSelf: 'center',
		},
		root: {
			display: 'flex',
			position: 'relative',
		},
		metadataText: {
			right: 0,
			position: 'absolute',
			margin: 'auto',
			padding: 10,
			marginRight: 5,
			display: 'flex',
		},
		metadataItemText: {
			paddingRight: '5px',
		},
		textLink: {
			textDecoration: 'unset',
			color: theme.palette.text.primary,
		},
	})
);

export interface ItemCardProps {
	item: PlaylistItem;
}
export function ItemCard(props: ItemCardProps) {
	const classes = useStyles();
	const [itemMedia, setItemMedia] = useState<string | false>(false);

	useEffect(() => {
		var cancelled = false;
		getItemMedia(props.item).then((url) => {
			if (!cancelled) {
				setItemMedia(url);
			}
		});
		return () => {
			cancelled = true;
		};
	}, [props.item]);

	return (
		<Card>
			<div className={classes.root}>
				{itemMedia && (
					<CardMedia
						className={classes.media}
						component="img"
						image={itemMedia}
						title="Album Art"
					/>
				)}
				<div className={classes.metadataText}>
					{isTrack(props.item) && props.item.explicit && (
						<Tooltip title="Contains explicit lyrics">
							<Typography
								variant="body1"
								className={classes.metadataItemText}
							>
								&#x1F92C;
							</Typography>
						</Tooltip>
					)}
					{hasRelease(props.item) && (
						<Tooltip title="Release Date">
							<Typography
								variant="body1"
								className={classes.metadataItemText}
							>
								&#x1F4C5;{' '}
								{getItemRelease(props.item)?.toDateString()}
							</Typography>
						</Tooltip>
					)}
					{isTrack(props.item) && (
						<Tooltip title="Relative Popularity">
							<Typography variant="body1">
								&#x1f525; {props.item.popularity}
							</Typography>
						</Tooltip>
					)}
				</div>
				<CardContent className={classes.content}>
					<Typography variant="h5" component="h2">
						<a
							href={getItemTitleLink(props.item)}
							className={classes.textLink}
						>
							{getItemTitle(props.item)}
						</a>
					</Typography>
					{!isArtist(props.item) && (
						<Typography variant="subtitle1" component="h2">
							<a
								href={getItemMainSubtitleLink(props.item)}
								className={classes.textLink}
							>
								{getItemMainSubtitle(props.item)}
							</a>
						</Typography>
					)}
					{isTrack(props.item) && (
						<Typography variant="subtitle2" component="h2">
							<a
								href={getItemSecondarySubtitleLink(props.item)}
								className={classes.textLink}
							>
								{getItemSecondarySubtitle(props.item)}
							</a>
						</Typography>
					)}
				</CardContent>
			</div>
		</Card>
	);
}
