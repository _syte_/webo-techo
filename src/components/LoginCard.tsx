import * as React from 'react';

import {
	Typography,
	makeStyles,
	createStyles,
	Theme,
	Card,
	CardContent,
	CardMedia,
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { spotify } from '../utils/SpotifyAPI';
import { useDispatch } from 'react-redux';
import { getUser } from '../redux/user';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
		},
		content: {
			flexGrow: 1,
			padding: theme.spacing(3),
		},
		rootCard: {
			display: 'flex',
			flexDirection: 'column',
			alignItems: 'center',
		},
		spotifyLoginButton: {
			color: 'green',
		},
		passwordLoginButton: {
			color: 'inherit',
		},
		loginFormRoot: {
			display: 'flex',
			flexDirection: 'column',
		},
		usernameInput: {
			marginTop: 10,
		},
		passwordInput: {
			marginTop: 10,
			marginBottom: 10,
		},
		media: {
			maxHeight: 150,
			width: 'auto',
			margin: 10,
		},
		margin: {
			margin: theme.spacing(1),
		},
	})
);

export function LoginCard() {
	const classes = useStyles();
	const dispatch = useDispatch();

	async function spotifyLogIn() {
		// window.location.href = await spotify.getAuthUrl();
		if (await spotify.obtainToken()) {
			dispatch(getUser());
		}
	}

	return (
		<>
			<Card>
				<CardContent>
					<div className={classes.rootCard}>
						<Typography variant="h2">Welcome to</Typography>
						<CardMedia
							className={classes.media}
							image="assets/SpotifyStats.webp"
							component="img"
							title="Spotify Stats Logo"
						/>
						<Button
							className={classes.margin}
							onMouseDown={spotifyLogIn}
							color="primary"
							size="large"
							variant="contained"
						>
							Login with Spotify
						</Button>

						<Typography
							variant="h5"
							align="center"
							className={classes.margin}
						>
							Spotify Stats is a website that lets you track your
							listening habits
							<br />
							and gives you meaningful visualisations of the way
							you choose to use the Spotify platform.
						</Typography>
					</div>
				</CardContent>
			</Card>
		</>
	);
}
