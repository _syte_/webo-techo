import * as React from 'react';

import { spotify } from '../utils/SpotifyAPI';
import { useState, useEffect } from 'react';
import {
	RadialAreaChart,
	ChartShallowDataShape,
	RadialAxis,
	RadialAreaSeries,
} from 'reaviz';
import {
	makeStyles,
	createStyles,
	Theme,
	Typography,
	useTheme,
} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
			justifyContent: 'space-evenly',
		},
		tempo: {
			display: 'flex',
			flexDirection: 'column',
			alignSelf: 'center',
			alignItems: 'center',
		},
	})
);

interface TrackFeaturesProps {
	track_id: string;
}

export function TrackFeatures(props: TrackFeaturesProps) {
	const classes = useStyles();
	const theme = useTheme();

	const [trackFeatures, setTrackFeatures] = useState<
		SpotifyApi.AudioFeaturesResponse
	>();

	useEffect(() => {
		var cancelled = false;
		spotify.getTrackAudioFeatures(props.track_id).then((track_features) => {
			if (!cancelled) {
				setTrackFeatures(track_features);
			}
		});
		return () => {
			cancelled = true;
		};
	}, [props.track_id]);

	let categoryData: ChartShallowDataShape[] = [
		{ key: 'Acousticness', data: trackFeatures?.acousticness ?? 0 },
		{ key: 'Danceability', data: trackFeatures?.danceability ?? 0 },
		{ key: 'Energy', data: trackFeatures?.energy ?? 0 },
		{ key: 'Instrumentalness', data: trackFeatures?.instrumentalness ?? 0 },
		{ key: 'Liveness', data: trackFeatures?.liveness ?? 0 },
		{ key: 'Valence', data: trackFeatures?.valence ?? 0 },
		// { key: 'Loudness', data: trackFeatures?.loudness ?? 0 },
		// { key: 'Speechiness', data: trackFeatures?.speechiness ?? 0 },
		// { key: 'Tempo', data: trackFeatures?.tempo ?? 0 },
	];
	return (
		<>
			{trackFeatures && (
				<div className={classes.root}>
					<RadialAreaChart
						data={categoryData}
						height={300}
						width={300}
						series={
							<RadialAreaSeries
								colorScheme={theme.palette.primary.main}
								animated
							/>
						}
						axis={<RadialAxis type="category" />}
					/>
					<div className={classes.tempo}>
						<Typography variant="h5" component="h2">
							Tempo
						</Typography>
						<Typography variant="body1">
							{trackFeatures.tempo.toFixed(0)} BPM
						</Typography>
					</div>
				</div>
			)}
		</>
	);
}
