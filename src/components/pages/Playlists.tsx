/// <reference types="react-vis-types" />
import * as React from 'react';

import {
	Typography,
	makeStyles,
	createStyles,
	List,
	ListItem,
	ListItemText,
	Card,
	CardContent,
	MenuItem,
	Menu,
	CardHeader,
	useTheme,
} from '@material-ui/core';
import { spotify } from '../../utils/SpotifyAPI';
import { useState, useEffect } from 'react';
import { Sunburst, SunburstPoint } from 'react-vis';
import { TrackFeatures } from '../TrackFeatures';
import { ItemCard, isTrack, PlaylistItem } from '../ItemCard';
import { useHistory } from 'react-router-dom';

interface PlaylistProps {
	playlist: SpotifyApi.PlaylistObjectSimplified;
}

function makeRandomColor(): string {
	const randn = () => {
		return Math.round(Math.random() * 255);
	};

	return `rgb(${randn()}, ${randn()}, ${randn()})`;
}

/**
 * Recursively work backwards from highlighted node to find path of valud nodes
 * @param {Object} node - the current node being considered
 * @returns {Array} an array of strings describing the key route to the current node
 */
function getKeyPath(node: SunburstPoint): Array<[string, string]> {
	if (!node.parent) {
		return [['root', 'root']];
	}
	const data: Array<[string, string]> = [
		[
			((node.data && node.data.id) || node.id) as string,
			((node.data && node.data.title) || node.title) as string,
		],
	];
	return data.concat(getKeyPath(node.parent));
}

/**
 * Recursively modify data depending on whether or not each cell has been selected by the hover/highlight
 * @param {Object} data - the current node being considered
 * @param {Object|Boolean} keyPath - a map of keys that are in the highlight path
 * if this is false then all nodes are marked as selected
 * @returns {Object} Updated tree structure
 */
function updateData(
	data: SunburstPoint,
	keyPath: Map<string, boolean> | false
): SunburstPoint {
	if (data.children) {
		data.children.map((child) => updateData(child, keyPath));
	}
	data.style = {
		...data.style,
		fill: data.color ?? data.data?.color,
		fillOpacity: keyPath && !keyPath.get(data.id) ? 0.2 : 1,
	};
	return data;
}

function calculateDuration(data: SunburstPoint): number {
	// console.log(`root ${data}`);
	if (data.children) {
		// console.log('Calculating children start');

		const result = data.children
			.map((c) => calculateDuration(c))
			.reduce((current, value) => current + value);
		// console.log(`Calculated children: ${result}`);
		return result;
	}

	if (data.size) {
		const result = data.size as number;
		// console.log(`Got size ${result}`);
		return result;
	}

	// console.log('Returning 0');

	return 0;
}

const useStyles = makeStyles(() =>
	createStyles({
		content: {
			display: 'flex',
			flexDirection: 'column',
			alignSelf: 'center',
		},
		root: {
			display: 'flex',
			justifyContent: 'space-evenly',
			alignItems: 'flex-start',
		},
		trackRoot: {
			display: 'flex',
			flexDirection: 'column',
			maxWidth: 600,
			minWidth: 600,
		},
		statDiv: {
			display: 'flex',
			flexDirection: 'column',
			// alignSelf: 'center',
			alignItems: 'center',
		},
		statsCard: {
			display: 'flex',
			flexDirection: 'row',
			justifyContent: 'space-evenly',
		},
		middleSunburstText: {
			position: 'absolute',
		},
		middleSuburstTextBody: {
			textAlign: 'center',
		},
		sunburst: { position: 'relative' },
		sunburstCard: {
			display: 'flex',
			position: 'relative',
			justifyContent: 'center',
			alignItems: 'center',
			'> text': {
				color: 'white',
			},
		},
	})
);

export interface StatItem {
	average: number;
	total: number;
}

// export interface PlaylistDurationStat {
// 	average: number;
// 	total: number;
// }

function howManyDaysAgo(oldDate: Date): number {
	const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds

	const today = new Date();

	return Math.round(Math.abs((+oldDate - +today) / oneDay));
}

function millisToMinutesAndSeconds(millis: number): string {
	var minutes = Math.floor(millis / 60000);
	var seconds = Math.floor((millis % 60000) / 1000);
	return seconds === 60
		? minutes + 1 + ':00'
		: minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
}

export function Playlist(props: PlaylistProps) {
	const classes = useStyles();
	const theme = useTheme();
	// const [tracks, setTracks] = useState<SpotifyApi.PlaylistTrackResponse>();
	const [trackData, setTrackData] = useState<SunburstPoint>();
	const [trackAgeStats, setTrackAgeStats] = useState<StatItem>();
	const [trackPopStats, setTrackPopStats] = useState<StatItem>();
	const [playlistDurationStats, setPlaylistDurationStats] = useState<
		StatItem
	>();
	const [selectedItem, setSelectedItem] = useState<PlaylistItem | false>(
		false
	);
	const [selectedDuration, setSelecetedDuration] = useState<number | false>(
		false
	);

	const [clicked, setClicked] = useState(false);

	useEffect(() => {
		setClicked(false);
	}, [props.playlist.id]);

	useEffect(() => {
		console.log(selectedItem);
	}, [selectedItem]);

	useEffect(() => {
		spotify.getPlaylistTracks(props.playlist.id).then((tracks) => {
			// setTracks(tracks);

			var artistDetails: Map<
				string,
				SpotifyApi.ArtistObjectSimplified
			> = new Map();
			var albumDetails: Map<
				string,
				SpotifyApi.AlbumObjectSimplified
			> = new Map();
			var trackDetails: Map<
				string,
				SpotifyApi.TrackObjectFull
			> = new Map();
			var data: Map<string, Map<string, string[]>> = new Map();

			var totalSongAge = 0.0;
			var totalSongPopularity = 0.0;
			var totalPlaylistDuration = 0.0;
			tracks.items.forEach((value) => {
				const track = value.track;
				const artist = track.artists[0];
				if (!artistDetails.has(artist.id)) {
					artistDetails.set(artist.id, artist);
				}

				const album = track.album;
				if (!albumDetails.has(album.id)) {
					albumDetails.set(album.id, album);
				}

				if (!data.has(artist.id)) {
					data.set(artist.id, new Map());
				}

				var albumData = data.get(artist.id);

				if (!albumData) {
					throw new Error();
				}

				if (!albumData.has(album.id)) {
					albumData.set(album.id, []);
				}

				if (!trackDetails.has(track.id)) {
					trackDetails.set(track.id, track);
				}

				var albumTracks = albumData.get(album.id);
				albumTracks?.push(track.id);

				/// Calculate stats on the tracks
				const trackDate = spotify.parseReleaseDate(
					track.album.release_date,
					track.album.release_date_precision
				);

				totalSongAge += howManyDaysAgo(trackDate);
				totalSongPopularity += track.popularity;
				totalPlaylistDuration += track.duration_ms;
			});

			setTrackAgeStats({
				average: totalSongAge / tracks.total,
				total: totalSongAge,
			});
			setTrackPopStats({
				average: totalSongPopularity / tracks.total,
				total: totalSongPopularity,
			});
			setPlaylistDurationStats({
				average: totalPlaylistDuration / tracks.total,
				total: totalPlaylistDuration,
			});
			// Use ts ignore to prevent errors on the should be allowed omission of size
			// @ts-ignore
			var sunburstData: SunburstPoint = {
				title: props.playlist.id,
				color: '#12939A',
				children: [],
			};

			data.forEach((value, artistId) => {
				const artist = artistDetails.get(artistId);
				// const size = Array.from(value.values())
				// 	.map((v) => v.length)
				// 	.reduce((current, value, i, a) => current + value);
				var albums: Array<SunburstPoint> = [];

				value.forEach((trackIds, albumId) => {
					const album = albumDetails.get(albumId);
					const albumTitle = album?.name ?? 'undefined';
					albums.push({
						id: albumId,
						title: albumTitle,
						itemData: album,
						// label: albumTitle,
						color: makeRandomColor(),
						// color: 'green',
						size: trackIds.length,
						children: trackIds.map((trackID) => {
							const trackDetail = trackDetails.get(trackID);
							return {
								id: trackID,
								itemData: trackDetail,
								track: true,
								title: trackDetail?.name,
								size: trackDetail?.duration_ms,
								color: makeRandomColor(),
							} as SunburstPoint;
						}),
					});
				});

				const artistTitle = artist?.name ?? 'undefined';
				// @ts-ignore
				sunburstData.children?.push({
					id: artistId,
					title: artistTitle,
					label: artistTitle,
					itemData: artist,
					color: makeRandomColor(),
					labelStyle: {
						...theme.typography.body2,
						pointerEvents: 'none',
						cursor: 'default',
						color: 'white',
					},

					// color: 'red',
					// size: size,
					children: albums,
				});
			});

			setTrackData(sunburstData);
		});
	}, [props.playlist.id, theme]);

	return (
		<>
			<div className={classes.root}>
				{trackData && (
					<Card className={classes.sunburstCard}>
						<Sunburst
							animation
							hideRootNode
							colorType="literal"
							className={classes.sunburst}
							onValueMouseOver={(node) => {
								if (clicked) {
									return;
								}
								const path: Array<[
									string,
									string
								]> = getKeyPath(node).reverse();
								const pathAsMap = path.reduce(
									(res, row: [string, string]) => {
										res.set(row[0], true);
										return res;
									},
									new Map<string, boolean>()
								);
								setTrackData(updateData(trackData, pathAsMap));
								if (node?.itemData || node?.data?.itemData) {
									const data =
										node.itemData ?? node.data.itemData;
									setSelectedItem(data);
									console.log('Calculating duration');
									console.log(node);
									setSelecetedDuration(
										calculateDuration(node)
									);
								}
							}}
							onValueMouseOut={() => {
								if (!clicked) {
									setTrackData(updateData(trackData, false));
									setSelectedItem(false);
									setSelecetedDuration(false);
								}
							}}
							onValueClick={(node) => {
								const path: Array<[
									string,
									string
								]> = getKeyPath(node).reverse();
								const pathAsMap = path.reduce(
									(res, row: [string, string]) => {
										res.set(row[0], true);
										return res;
									},
									new Map<string, boolean>()
								);
								setTrackData(updateData(trackData, pathAsMap));
								if (node?.itemData || node?.data?.itemData) {
									const data =
										node.itemData ?? node.data.itemData;
									setSelectedItem(data);
									setSelecetedDuration(
										calculateDuration(node)
									);
								}
								setClicked(!clicked);
							}}
							data={trackData}
							height={500}
							width={500}
						/>
						{playlistDurationStats && (
							<div className={classes.middleSunburstText}>
								<Typography
									variant="h5"
									component="h2"
									className={classes.middleSuburstTextBody}
								>
									{selectedDuration
										? `${
												Math.round(
													(selectedDuration /
														playlistDurationStats.total) *
														100 *
														100
												) / 100
										  }%`
										: ''}
								</Typography>
								<Typography
									variant="h5"
									component="h2"
									className={classes.middleSuburstTextBody}
								>
									{selectedDuration
										? millisToMinutesAndSeconds(
												selectedDuration
										  ) + '/'
										: ''}
									{`${millisToMinutesAndSeconds(
										playlistDurationStats?.total
									)} mins`}
								</Typography>
							</div>
						)}
					</Card>
				)}

				<div className={classes.trackRoot}>
					{selectedItem ? (
						<>
							<ItemCard item={selectedItem} />
							{/* {!clicked && isTrack(selectedItem) && (
											<Typography variant="subtitle2">
												Click on the track to view more
												information
											</Typography>
										)} */}
							{clicked && isTrack(selectedItem) && (
								<Card>
									<CardContent>
										<TrackFeatures
											track_id={selectedItem.id}
										/>
									</CardContent>
								</Card>
							)}
							{isTrack(selectedItem) && (
								<Card>
									<CardContent className={classes.statsCard}>
										{trackAgeStats && (
											<div className={classes.statDiv}>
												<Typography
													variant="h5"
													component="h2"
												>
													Age
												</Typography>
												<Typography variant="body1">
													{howManyDaysAgo(
														spotify.parseReleaseDate(
															selectedItem.album
																.release_date,
															selectedItem.album
																.release_date_precision
														)
													)}{' '}
													days
												</Typography>
												<Typography variant="body1">
													Avg:{' '}
													{Math.round(
														trackAgeStats.average
													)}{' '}
													days
												</Typography>
											</div>
										)}
										{trackPopStats && (
											<div className={classes.statDiv}>
												<Typography
													variant="h5"
													component="h2"
												>
													Popularity
												</Typography>
												<Typography variant="body1">
													&#x1f525;{' '}
													{selectedItem.popularity}
												</Typography>
												<Typography variant="body1">
													Avg: &#x1f525;
													{Math.round(
														trackPopStats.average
													)}
												</Typography>
											</div>
										)}
										{playlistDurationStats &&
											isTrack(selectedItem) && (
												<div
													className={classes.statDiv}
												>
													<Typography
														variant="h5"
														component="h2"
													>
														Duration
													</Typography>
													<Typography variant="body1">
														{millisToMinutesAndSeconds(
															selectedItem.duration_ms
														)}{' '}
														min:sec
													</Typography>
													<Typography variant="body1">
														Avg:{' '}
														{millisToMinutesAndSeconds(
															playlistDurationStats.average
														)}
													</Typography>
													<Typography variant="body1">
														{Math.round(
															(selectedItem.duration_ms /
																playlistDurationStats.total) *
																100 *
																100
														) / 100}{' '}
														% of the playlist
														duration
													</Typography>
												</div>
											)}
									</CardContent>
								</Card>
							)}
						</>
					) : (
						<Card>
							<CardContent className={classes.content}>
								<Typography variant="h5" component="h2">
									Select an item on the left to get started!
								</Typography>
								<Typography variant="h5" component="h2">
									You can click on a track (outer ring) to see
									additional information about it
								</Typography>
							</CardContent>
						</Card>
					)}
				</div>
			</div>
		</>
	);
}

interface PlaylistsProps {
	playlist_id?: string;
}

export function Playlists(props: PlaylistsProps) {
	let history = useHistory();
	// let match = useRouteMatch();
	// const theme = useSelector((state: RootState) => state.theme);
	const [index, setIndex] = useState<number | undefined>();
	const hasIndex = (index ?? -1) >= 0;
	const [playlists, setPlaylists] = useState<
		SpotifyApi.ListOfUsersPlaylistsResponse
	>();
	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
	useEffect(() => {
		spotify.getUserPlaylists().then((playlists) => {
			setPlaylists(playlists);
			console.log(playlists);
		});
	}, []);

	useEffect(() => {
		if (props.playlist_id) {
			if (playlists) {
				playlists.items.forEach((element, i) => {
					if (element.id === props.playlist_id) {
						setIndex(i);
						return;
					}
				});
			}
		} else {
			setIndex(0);
		}
	}, [props.playlist_id, playlists]);

	const handleClickListItem = (event: React.MouseEvent<HTMLElement>) => {
		setAnchorEl(event.currentTarget);
	};

	const handleMenuItemClick = (
		event: React.MouseEvent<HTMLElement>,
		index: number
	) => {
		setIndex(index);
		setAnchorEl(null);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	const activePlaylist = hasIndex && playlists?.items[index!];
	return (
		<>
			{/* <Grid
				container
				direction="row"
				justify="flex-start"
				alignItems="stretch"
			> */}
			<Card>
				<CardContent>
					<div style={{ display: 'flex' }}>
						<CardHeader title="Playlist Sunburst" />
						{hasIndex && (
							<List component="nav" aria-label="Playlist setting">
								<ListItem
									button
									aria-haspopup="true"
									aria-controls="lock-menu"
									aria-label="playlist"
									onClick={handleClickListItem}
								>
									<ListItemText
										primary="Playlist"
										secondary={
											playlists?.items[index!].name
										}
									/>
								</ListItem>
							</List>
						)}
						<Menu
							id="lock-menu"
							anchorEl={anchorEl}
							keepMounted
							open={Boolean(anchorEl)}
							onClose={handleClose}
						>
							{playlists?.items.map((option, i) => (
								<MenuItem
									key={option.id}
									selected={i === index}
									onClick={(event) => {
										handleMenuItemClick(event, i);
										history.push(
											'/playlists?pid=' + option.id
										);
									}}
								>
									{option.name}
								</MenuItem>
							))}
						</Menu>
					</div>
					{activePlaylist && <Playlist playlist={activePlaylist} />}
				</CardContent>
			</Card>
		</>
	);
}
