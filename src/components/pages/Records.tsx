/// <reference types="react-vis-types" />
import * as React from 'react';

import {
	ChartShallowDataShape,
	RadialAxis,
	RadialAreaChart,
	RadialAxisArcSeries,
	RadialAreaSeries,
} from 'reaviz';

import {
    makeStyles,
    createStyles,
    Theme,
    CardContent,
    Card,
    CardHeader,
    List,
    ListItem,
    ListItemText,
    Typography,
    Menu,
    MenuItem,
	useTheme,
} from '@material-ui/core';
import { spotify } from '../../utils/SpotifyAPI';
import { useState, useEffect } from 'react';
import { ItemCard } from "../ItemCard";

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
			justifyContent: 'space-evenly',
		},
		list: {
			width: '100%',
			maxWidth: '36ch',
			backgroundColor: theme.palette.background.paper,
		},
		inline: {
			display: 'inline',
		},
		textLink: {
			display: 'inline',
			textDecoration: 'unset',
			color: theme.palette.text.primary,
		},
		statDiv: {
			display: 'flex',
			flexDirection: 'column',
			// alignSelf: 'center',
			alignItems: 'center',
		},
		statsCard: {
			display: 'flex',
			flexDirection: 'row',
			justifyContent: 'space-evenly',
		},
	})
);

interface genresProps {
	topGenres: Map<string, number>;
}

interface artistsProps {
    topArtists: SpotifyApi.UsersTopArtistsResponse;
}

export function TopRecords() {
	const classes = useStyles();

	const [topArtists, setTopArtists] = useState<
		SpotifyApi.UsersTopArtistsResponse
	>();

	useEffect(() => {
		spotify.getMyTopArtists().then((artists) => setTopArtists(artists));
	}, []);

	let topGenres: Map<string, number> = new Map();

    topArtists?.items.forEach((artist) => {
        artist.genres.forEach((genre) => {
            topGenres.set(genre, (topGenres?.get(genre) ?? 0) + 1);
        });
    });

    return (
        <div className={classes.root}>
            <TopGenres topGenres={topGenres}/>
            {topArtists && <TopArtists topArtists={topArtists}/>}
        </div>
    );
}

export function TopArtists(props: artistsProps){
    const classes = useStyles();
    const [index, setIndex] = useState<number>(0);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const handleClickListItem = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleMenuItemClick = (
        event: React.MouseEvent<HTMLElement>,
        index: number
    ) => {
        setIndex(index);
        setAnchorEl(null);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className={classes.root}>
            <Card>
                <CardHeader
                    title="Top Artists Details"
                />
                <CardContent>
                    <List component="nav" aria-label="Top Artists setting">
                        <ListItem
                            button
                            aria-haspopup="true"
                            aria-controls="lock-menu"
                            aria-label="topArtist"
                            onClick={handleClickListItem}
                        >
                            <ListItemText
                                primary="Artist"
                                secondary={props.topArtists.items[index].name}
                            />
                        </ListItem>
                    </List>
                    <Menu
                        id="lock-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                    >
                        {props.topArtists.items.map((option, i) => (
                            <MenuItem
                                key={option.id}
                                selected={i === index}
                                onClick={(event) => {
                                    handleMenuItemClick(event, i);
                                }}
                            >
                                {option.name}
                            </MenuItem>
                        ))}
                    </Menu>
                    <ItemCard item={props.topArtists.items[index]} />
					<Card>
						<CardContent className={classes.statsCard}>
							<div className={classes.statDiv}>
								<Typography
									variant="h5"
									component="h2"
								>
									Followers
								</Typography>
								<Typography variant="body1">
									{props.topArtists.items[index].followers.total}
								</Typography>
							</div>
							<div className={classes.statDiv}>
								<Typography
									variant="h5"
									component="h2"
								>
									Popularity
								</Typography>
								<Typography variant="body1">
									&#x1f525;{' '}
									{props.topArtists.items[index].popularity}
								</Typography>
							</div>
						</CardContent>
					</Card>
                </CardContent>
            </Card>
        </div>
    );
}

export function TopGenres(props: genresProps) {
	const classes = useStyles();
	const theme = useTheme();

	let genreData: ChartShallowDataShape[] = [];

	props.topGenres.forEach((value, key) => {
		genreData.push({ key: key, data: value });
	});

	return (
		<div className={classes.root}>
			<Card>
				<CardHeader title="Top Genres Graph" />
				<CardContent>
					<RadialAreaChart
						height={600}
						width={600}
						data={genreData}
						series={
							<RadialAreaSeries
								colorScheme={theme.palette.primary.main}
								animated
							/>
						}
						axis={
							<RadialAxis
								type="category"
								arcs={
									<RadialAxisArcSeries
										count={genreData.length}
									/>
								}
							/>
						}
					/>
				</CardContent>
			</Card>
		</div>
	);
}

export function Records() {
	return (
		<>
			<Card>
				<CardContent>
					<TopRecords />
				</CardContent>
			</Card>
		</>
	);
}
