import * as React from 'react';

import {
	makeStyles,
	createStyles,
	Theme,
	Card,
	CardContent,
	CardHeader,
} from '@material-ui/core';

import { spotify } from '../../utils/SpotifyAPI';
import { useState, useEffect } from 'react';
import { CalendarHeatmap, ChartShallowDataShape, ChartDataTypes } from 'reaviz';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
			justifyContent: 'space-evenly',
		},
		cardStack: {
			display: 'flex',
			flexDirection: 'column',
		},
	})
);

export function RecentlyPlayed() {
	const classes = useStyles();

	const [recentlyPlayed, setRecentlyPlayed] = useState<
		SpotifyApi.UsersRecentlyPlayedTracksResponse
	>();

	useEffect(() => {
		spotify.getMyRecentlyPlayedTracks().then((recent) => {
			setRecentlyPlayed(recent);
			console.log(recent);
		});
	}, []);

	// Map<Date, Map<Name, Amount>>
	let recentlyPlayedTracks: Map<string, Map<string, number>> = new Map();

	recentlyPlayed?.items.forEach((recent) => {
		let dateOnly = new Date(recent.played_at).toDateString();
		let map = recentlyPlayedTracks?.get(dateOnly) ?? new Map();
		map.set(recent.track.name, (map?.get(recent.track.name) ?? 0) + 1);
		recentlyPlayedTracks.set(dateOnly, map);
	});

	let data: ChartShallowDataShape<ChartDataTypes>[] = [];

	recentlyPlayedTracks.forEach((map, date) => {
		let amountPerDate = 0;
		map.forEach((amount, name) => {
			amountPerDate += amount;
		});
		data.push({ key: new Date(date), data: amountPerDate });
	});

	return (
		<div className={classes.cardStack}>
			<Card>
				<CardContent>
					<CardHeader title="Recently Played Songs Calendar" />
					<div className={classes.root}>
						{recentlyPlayed && (
							<CalendarHeatmap
								height={120}
								width={700}
								data={data}
							/>
						)}
					</div>
				</CardContent>
			</Card>
		</div>
	);
}
