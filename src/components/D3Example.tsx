import React, {useEffect, useRef, useState} from 'react'
import { select, Selection } from 'd3-selection'
import { scaleLinear, scaleBand } from 'd3-scale'
import { max } from 'd3-array'
import { axisLeft, axisBottom } from 'd3-axis'

const startData = [
    {
        name: 'a',
        amount: 9000,
    },
    {
        name: 'b',
        amount: 5235,
    },
    {
        name: 'c',
        amount: 5298,
    },
    {
        name: 'd',
        amount: 12904,
    },
    {
        name: 'e',
        amount: 9471,
    },
];

const dimensions = {
    chartWidth: 700,
    chartHeight: 400,
    margin: 50,
}

export function D3Example(){
    const ref = useRef<SVGSVGElement | null>(null);
    const [selection, setSelection] = useState<null | Selection<
        SVGSVGElement | null,
        unknown,
        null,
        undefined
        >>(null);
    const [data, setData] = useState(startData);

    const maxValue = max(data, d => d.amount);

    const y = scaleLinear()
        .domain([0, maxValue!])
        .range([dimensions.chartHeight, 0]);

    const x = scaleBand()
        .domain(data.map(d => d.name))
        .range([0, dimensions.chartWidth])
        .paddingOuter(0.05)
        .paddingInner(0.05);

    const yAxis = axisLeft(y)
        .ticks(3)
        .tickFormat(d => `$${d}`);
    const xAxis = axisBottom(x);

    useEffect(() => {
        if(!selection) {
            setSelection(select(ref.current))
        }
        else {
            const xAxisGroup = selection
                .append('g')
                .attr('transform', `translate(${dimensions.margin}, ${dimensions.chartHeight + dimensions.margin})`)
                .call(xAxis);

            const yAxisGroup = selection
                .append('g')
                .attr('transform', `translate(${dimensions.margin}, ${dimensions.margin})`)
                .call(yAxis);

            selection
                .append('g')
                .attr('transform', `translate(${dimensions.margin}, ${dimensions.margin})`)
                .selectAll('rect')
                .data(data)
                .enter()
                .append('rect')
                .attr('width', x.bandwidth)
                .attr('x', d => {
                    const xVal = x(d.name);
                    if(xVal)
                        return xVal;
                    return null;
                })
                .attr('y', d => y(d.amount))
                .attr('fill', 'orange')
                .attr('height', d => dimensions.chartHeight - y(d.amount));
        }
    }, [selection]);

    return (
        <div>
            <svg ref={ref} width={dimensions.chartWidth + 2 * dimensions.margin} height={dimensions.chartHeight + 2 * dimensions.margin}/>
        </div>
    );
}
