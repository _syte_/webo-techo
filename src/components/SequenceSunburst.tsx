import React, {useEffect, useRef, useState} from 'react'
import { select, selectAll, Selection } from 'd3-selection'
import { scaleLinear, scaleOrdinal } from 'd3-scale'
import { schemeCategory10 } from 'd3-scale-chromatic'
import { partition, hierarchy, HierarchyRectangularNode } from 'd3-hierarchy'
import { arc } from 'd3-shape'

export function SequenceSunburst(data: any) {
    // const ref = useRef<SVGSVGElement | null>(null);
    // const [selection, setSelection] = useState<null | Selection<
    //     SVGSVGElement | null,
    //     unknown,
    //     null,
    //     undefined
    //     >>(null);

    const width = 750;
    const height = 600;

    const radius = Math.min(width, height) / 2;

    const x = scaleLinear()
        .range([0, 2 * Math.PI]);

    const y = scaleLinear()
        .range([0, radius])

    const breadcrumbDims = {
        width: 75,
        height: 30,
        spacing: 3,
        widthOfTail: 10,
    };

    const colors = scaleOrdinal(schemeCategory10);
    let totalSize = 0;

    const vis = select('#chart')
        .append('svg:svg')
        .attr('width', width)
        .attr('height', height)
        .append('svg:g')
        .attr('id', 'container')
        .attr('transform', `translate(${width / 2}, ${height / 2})`);

    const part = partition();

    function mouseover(d: any) {
        const percentage = (100 * d.value / totalSize).toPrecision(3);
        let percentageString = percentage + '%';
        if (+percentage < 0.1) {
            percentageString = '< 0.1%';
        }
        select('#percentage')
            .text(percentageString);
        select('#explanation')
            .style('visibility', '');
        const sequenceArray = getAncestors(d);
        updateBreadcrumbs(sequenceArray, percentageString);
        selectAll('path')
            .style('opacity', 0.3);
        vis.selectAll('path')
            .filter(function(node) {
                return (sequenceArray.indexOf(node) >= 0);
            })
            .style('opacity', 1);
    }

    function mouseleave(d: any) {
        select('#trail')
            .style('visibility', 'hidden');
        selectAll('path').on('mouseover', null);
        selectAll('path')
            .transition()
            .duration(1000)
            .style('opacity', 1)
            .on('end', function() {
                select(this).on('mouseover', mouseover);
                select('#percentage').text('');
            });
        select('#explanation')
            .style('visibility', 'hidden');
    }

    function getAncestors(node: any) {
        let path = [];
        let current = node;
        while (current.parent) {
            path.unshift(current);
            current = current.parent;
        }
        return path;
    }

    function initializeBreadcrumbTrail() {
        const trail = select('#sequence').append('svg:svg')
            .attr('width', width)
            .attr('height', 50)
            .attr('id', 'trail');
        // Add the label at the end, for the percentage.
        trail.append('svg:text')
            .attr('id', 'endlabel')
            .style('fill', '#000');
    }

    function breadcrumbPoints(d: any, i: any) {
        let points = [];
        points.push('0,0');
        points.push(breadcrumbDims.width + ',0');
        points.push(breadcrumbDims.width + breadcrumbDims.widthOfTail + ',' + (breadcrumbDims.height / 2));
        points.push(breadcrumbDims.width + ',' + breadcrumbDims.height);
        points.push('0,' + breadcrumbDims.height);
        if (i > 0) { // Leftmost breadcrumb; don't include 6th vertex.
            points.push(breadcrumbDims.widthOfTail + ',' + (breadcrumbDims.height / 2));
        }
        return points.join(' ');
    }

    function updateBreadcrumbs(nodeArray: any, percentageString: any) {
        // Data join; key function combines name and depth (= position in sequence).
        const g = select('#trail')
            .selectAll('g')
            // @ts-ignore
            .data(nodeArray, function(d) { return d.data.name + d.depth});

        const entering = g.enter().append('g');

        entering.append('svg:polygon')
            .attr('points', breadcrumbPoints)
            .style('fill', function(d) {
                // @ts-ignore
                return colors((d.children ? d : d.parent).data.name);
            });

        entering.append('svg:text')
            .attr('x', (breadcrumbDims.width + breadcrumbDims.widthOfTail) / 2)
            .attr('y', breadcrumbDims.height / 2)
            .attr('dy', '0.35em')
            .attr('text-anchor', 'middle')
            // @ts-ignore
            .text(function(d) { return d.data.name; });

        select('#trail')
            .selectAll('g').attr('transform', function(d, i) {
            // @ts-ignore
            return `translate(${(d.depth - 1) * (breadcrumbDims.width + breadcrumbDims.spacing)}, 0)`;
        });

        g.exit().remove();

        select('#trail').select('#endlabel')
            .attr('x', (nodeArray.length + 0.5) * (breadcrumbDims.width + breadcrumbDims.spacing))
            .attr('y', breadcrumbDims.height / 2)
            .attr('dy', '0.35em')
            .attr('text-anchor', 'middle')
            .text(percentageString);

        select('#trail')
            .style('visibility', '');

    }

    useEffect(() => {
        initializeBreadcrumbTrail();
        vis.append('svg:circle')
            .attr('r', radius)
            .style('opacity', 0);
        const root = hierarchy(data)
            .sum(d => d.size);
        const nodes = part(root).descendants();

        let maxDepth = 0;
        const path = vis.data(nodes)
            .selectAll('path')
            .data(nodes)
            .enter()
            .append('svg:path')
            .attr('display', d => d.depth ? null : "none")
            // @ts-ignore
            .attr('d', function(d) {
                arc()
                    .startAngle(Math.max(0, Math.min(2 * Math.PI, x(d.x0))))
                    .endAngle(Math.max(0, Math.min(2 * Math.PI, x(d.x1))))
                    .innerRadius(Math.max(0, y(d.y0)))
                    .outerRadius(Math.max(0, y(d.y1)));
            })
            .attr('fill-rule', 'evenodd')
            .style('fill', function(d: HierarchyRectangularNode<any>) {
                if (d.depth > maxDepth) {
                    maxDepth = d.depth;
                }
                return colors((d.children ? d : d.parent)?.data.name);
            })
            .style('opacity', 1)
            .on('mouseover', mouseover);

    });

    return (
        <div>
            <div id="main">
                <div id="sequence"/>
                <div id="chart"/>
            </div>
        </div>
        // <div>
        //     <svg ref={ref} width={width} height={height}>
        //
        //     </svg>
        // </div>
    )
}
