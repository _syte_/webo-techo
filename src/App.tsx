import * as React from 'react';

import {
	ThemeProvider,
	FormControlLabel,
	AppBar,
	Toolbar,
	Typography,
	makeStyles,
	createStyles,
	Theme,
	CssBaseline,
	Checkbox,
	BottomNavigation,
	BottomNavigationAction,
} from '@material-ui/core';
import QueueMusicIcon from '@material-ui/icons/QueueMusic';
import TimelineIcon from '@material-ui/icons/Timeline';
// import AlbumIcon from '@material-ui/icons/Album';
import { mainTheme, altTheme } from './utils/theme';
import { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useUser } from './redux/user';
import { RootState } from './redux/rootReducer';
import { toggleTheme, AppTheme } from './redux/theme';
import {
	HashRouter as Router,
	Route,
	Switch,
	useHistory,
	useLocation,
} from 'react-router-dom';
// import { RecentlyPlayed } from './components/pages/RecentlyPlayed';
import { Playlists } from './components/pages/Playlists';
import { Records } from './components/pages/Records';
import { LoginCard } from './components/LoginCard';

export type AppState = {
	theme: boolean;
	isSignedIn: boolean;
	displayName: string;
};

const bottomNavHeight = '4em';
const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		menuButton: {
			marginRight: theme.spacing(2),
		},
		appRoot: {
			// maxHeight: '100vh',
			// maxWidth: '100vw',
			// overflow: 'auto',
		},
		root: {
			display: 'flex',
			// backgroundColor: theme.palette.background.default,
		},
		appBar: {
			zIndex: theme.zIndex.drawer + 1,
		},
		content: {
			flexGrow: 1,
			padding: theme.spacing(3),
			marginBottom: bottomNavHeight,
		},
		title: {
			flexGrow: 1,
		},
		routerLink: {
			textDecoration: 'none',
			color: theme.palette.text.primary,
		},
		active: {
			color: theme.palette.primary.light,
		},
		media: {
			height: '5em',
			paddingRight: '1em',
		},
		bottom_nav: {
			position: 'fixed',
			bottom: '0px',
			width: '100%',
			height: bottomNavHeight,
		},
	})
);

export function AppRoot() {
	const classes = useStyles();
	const dispatch = useDispatch();
	const user = useUser();

	const theme = useSelector((state: RootState) => state.theme);
	const themeToggle = useCallback(() => dispatch(toggleTheme()), [dispatch]);

	return (
		<div className={classes.root}>
			<CssBaseline />
			<AppBar position="fixed" className={classes.appBar}>
				<Toolbar>
					<img
						className={classes.media}
						src="assets/logo.webp"
						alt="Logo"
					/>
					<Typography variant="h6" className={classes.title}>
						Spotify Stats
					</Typography>
					<FormControlLabel
						control={
							<Checkbox
								checked={theme === AppTheme.Light}
								onChange={themeToggle}
								value="theme"
								color="secondary"
							/>
						}
						label="Dark Theme"
					/>
					{user && <p>{user.displayName}</p>}
				</Toolbar>
			</AppBar>

			{user ? (
				<Router>
					<Navigation />
				</Router>
			) : (
				<div className={classes.content}>
					<Toolbar />
					<LoginCard />
				</div>
			)}
		</div>
	);
}

const navLinks = ['/playlists', '/records'];

function Navigation() {
	const classes = useStyles();
	const location = useLocation();
	const history = useHistory();

	const [selectedIndex, setSelectedIndex] = useState(() => {
		if (location.pathname === '/') {
			// Default to first entry
			return 0;
		} else {
			let find = navLinks.indexOf(location.pathname);
			if (find === -1) {
				console.warn(
					`Could find nav link index for path: ${location.pathname}`
				);
				return 0;
			} else {
				return find;
			}
		}
	});

	return (
		<>
			<ChildContent />
			<BottomNavigation
				value={selectedIndex}
				onChange={(_, newValue) => {
					setSelectedIndex(newValue);
					history.push(navLinks[newValue]);
				}}
				showLabels
				className={classes.bottom_nav}
			>
				<BottomNavigationAction
					label="Playlists"
					icon={<QueueMusicIcon />}
				/>
				<BottomNavigationAction
					label="Records"
					icon={<TimelineIcon />}
				/>
			</BottomNavigation>
		</>
	);
}

function useQuery() {
	return new URLSearchParams(useLocation().search);
}

function ChildContent() {
	const classes = useStyles();
	let query = useQuery();
	return (
		<div className={classes.content}>
			<Toolbar />
			<Switch>
				<Route path="/records" component={Records} />
				{/* <Route path="/recent" component={RecentlyPlayed!} /> */}
				{/* This needs to be last otherwise the / entry will take everything in */}
				<Route path={['/', '/playlists', '/playlists?pid=:playlistId']}>
					<Playlists playlist_id={query.get('pid') ?? undefined} />
				</Route>
			</Switch>
		</div>
	);
}

export function App() {
	const theme = useSelector((state: RootState) => state.theme);
	const classes = useStyles();

	return (
		<div className={classes.appRoot}>
			<ThemeProvider
				theme={theme === AppTheme.Light ? mainTheme : altTheme}
			>
				<CssBaseline />
				<AppRoot />
			</ThemeProvider>
		</div>
	);
}
