# Webo Techo

## Building

### Dependencies

-   [Yarn](https://yarnpkg.com/)

### Frontend

To install all dependencies and get your environment ready, run yarn:

```
yarn
```

To run the development setup, where all Typescript files will be watched(In the frontend and backend) for changes run this:

```
yarn run dev
```

After waiting for a minute for everything to be ready browse to `localhost:3000`